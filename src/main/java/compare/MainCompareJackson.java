package compare;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class MainCompareJackson {

	public static void main(String[] args) throws IOException {
		// Load Json from API to Map
		String jsonFromAPIFileName = "resources/JsonFromAPIJackson.txt";
		Path jsonFromAPIPath = Paths.get(jsonFromAPIFileName).toAbsolutePath();
		String jsonfromAPI = Files.lines(jsonFromAPIPath).collect(Collectors.joining("\n"));

		// Load Json Expected to Map
		String jsonExpectedFileName = "resources/JsonExpectedJackson.txt";
		Path jsonExpectedPath = Paths.get(jsonExpectedFileName).toAbsolutePath();
		String jsonExpected = Files.lines(jsonExpectedPath).collect(Collectors.joining("\n"));
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonNodeAPI = mapper.readTree(jsonfromAPI);
		JsonNode jsonNodeExpected = mapper.readTree(jsonExpected);
		
		boolean result = jsonNodeAPI.equals(jsonNodeExpected);
		System.out.println(result);
	}
}
