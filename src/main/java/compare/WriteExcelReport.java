package compare;

import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcelReport {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Test result");
		generateHeader(sheet);
		String failedReason = "Some field is not correct1 \nSome field is not correct2";
		writeResult(sheet, "Testcase-01", "Check JSON", true, "", 1);
		writeResult(sheet, "Testcase-02", "Check JSON", false, failedReason, 2);
		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		FileOutputStream outputStream = new FileOutputStream("D:\\Automation Test\\Jmeter test result\\TestResult.xlsx");
		workbook.write(outputStream);
		workbook.close();
		System.out.println("Done");
	}
	
	public static void writeResult(XSSFSheet sheet, String testID, String testName, boolean isTestCasePassed, String reason, int rowIndex) {
		CellStyle testInfoCellStyle = sheet.getWorkbook().createCellStyle();
		testInfoCellStyle.setBorderBottom(BorderStyle.THIN);
		testInfoCellStyle.setBorderLeft(BorderStyle.THIN);
		testInfoCellStyle.setBorderRight(BorderStyle.THIN);
		testInfoCellStyle.setBorderTop(BorderStyle.THIN);
		testInfoCellStyle.setWrapText(true);
		Row testCaseResult = sheet.createRow(rowIndex);
		Cell testIDCell = testCaseResult.createCell(0);
		testIDCell.setCellValue(testID);
		testIDCell.setCellStyle(testInfoCellStyle);
		
		Cell testNameCell = testCaseResult.createCell(1);
		testNameCell.setCellValue(testName);
		testNameCell.setCellStyle(testInfoCellStyle);
		
		Cell testReasonCell = testCaseResult.createCell(3);
		testReasonCell.setCellValue(reason);
		testReasonCell.setCellStyle(testInfoCellStyle);
		
		Cell testResult = testCaseResult.createCell(2);
		CellStyle resultCellStyle = sheet.getWorkbook().createCellStyle();
		resultCellStyle.setBorderBottom(BorderStyle.THIN);
		resultCellStyle.setBorderLeft(BorderStyle.THIN);
		resultCellStyle.setBorderRight(BorderStyle.THIN);
		resultCellStyle.setBorderTop(BorderStyle.THIN);
		resultCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		if(isTestCasePassed) {
			testResult.setCellValue("Pass");
			resultCellStyle.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.index);
			testResult.setCellStyle(resultCellStyle);
		}else {
			testResult.setCellValue("Fail");
			resultCellStyle.setFillForegroundColor(IndexedColors.RED.index);
			testResult.setCellStyle(resultCellStyle);
		}
	}
	
	public static void generateHeader(XSSFSheet sheet) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setFillForegroundColor(IndexedColors.ORANGE.index);
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		Row header = sheet.createRow(0);
		Cell testID = header.createCell(0);
		testID.setCellValue("Test case ID");
		testID.setCellStyle(headerCellStyle);
		
		Cell testName = header.createCell(1);
		testName.setCellValue("Test case Name");
		testName.setCellStyle(headerCellStyle);
		
		Cell testResult = header.createCell(2);
		testResult.setCellValue("Result");
		testResult.setCellStyle(headerCellStyle);
		
		Cell testReason = header.createCell(3);
		testReason.setCellValue("Reason");
		testReason.setCellStyle(headerCellStyle);
	}
	
}
