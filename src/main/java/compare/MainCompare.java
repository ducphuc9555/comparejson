package compare;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

public class MainCompare {

	public static void main(String[] args) throws IOException {
		// Create excel file for report
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Test result");
		generateHeader(sheet);
		String folderPath = System.getProperty("user.dir");
		String outputPath = folderPath + "\\TestResult.xlsx";

		String failedReason = "";
		boolean isTestCasePassed = true;
		int reportRowIndex = 1;
		// Load Json from API to Map
		String jsonFromAPIFileName = "resources/JsonFromAPIJackson.txt";
		Path jsonFromAPIPath = Paths.get(jsonFromAPIFileName).toAbsolutePath();
		String jsonfromAPI = Files.lines(jsonFromAPIPath).collect(Collectors.joining("\n"));
		JSONObject jsonFromAPIObject = new JSONObject(jsonfromAPI);
		Map<String, Object> jsonFromAPIItemList = handleJSONObject(jsonFromAPIObject, "");

		// Load Json Expected to Map
		String jsonExpectedFileName = "resources/JsonExpectedJackson.txt";
		Path jsonExpectedPath = Paths.get(jsonExpectedFileName).toAbsolutePath();
		String jsonExpected = Files.lines(jsonExpectedPath).collect(Collectors.joining("\n"));
		JSONObject jsonExpectedObject = new JSONObject(jsonExpected);
		Map<String, Object> jsonExpectedItemList = handleJSONObject(jsonExpectedObject, "");
		
//		for (Map.Entry<String, Object> jsonExpectedItemListMap : jsonExpectedItemList.entrySet()) {
//			String keyName = jsonExpectedItemListMap.getKey();
//			Object keyValue = jsonExpectedItemListMap.getValue();
//			System.out.println(keyName + ": " + keyValue);
//		}

		for (Map.Entry<String, Object> jsonExpectedItemListMap : jsonExpectedItemList.entrySet()) {
			String keyName = jsonExpectedItemListMap.getKey();
			Object valueFromAPI = jsonFromAPIItemList.get(keyName);
			Object valueFromExpected = jsonExpectedItemListMap.getValue();
			String valueFromAPIDataType = valueFromAPI.getClass().getName();
			String valueExptectedDataType = valueFromExpected.getClass().getName();
			if(valueFromExpected instanceof JSONArray) {
				boolean result = isArrayEquals((JSONArray)valueFromExpected, (JSONArray)valueFromAPI);
				if(!result) {
					String error = keyName + " has incorrect value. Actual : " + valueFromAPI + " - " + "Expected: " + valueFromExpected;
					failedReason += error + "\n";
					isTestCasePassed = false;
					System.out.println(error);
				}
				continue;
			}
			if (!valueExptectedDataType.equals(valueFromAPIDataType)) {
				String error = keyName + " has incorrect data type. Actual : " + valueFromAPIDataType + " - " + "Expected: " + valueExptectedDataType;
				failedReason += error + "\n";
				isTestCasePassed = false;
				System.out.println(error);
			}
			boolean isValueCorrect = Objects.equals(valueFromAPI, valueFromExpected);
			if (!isValueCorrect) {
				String error = keyName + " has incorrect value. Actual : " + valueFromAPI + " - " + "Expected: " + valueFromExpected;
				failedReason += error + "\n";
				isTestCasePassed = false;
				System.out.println(error);
			}
		}
		writeResult(sheet, "TestCase-01", "Compare JSON", isTestCasePassed, failedReason.trim(), reportRowIndex);
		reportRowIndex++;

		sheet.autoSizeColumn(0);
		sheet.autoSizeColumn(1);
		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);
		FileOutputStream outputStream = new FileOutputStream(outputPath);
		workbook.write(outputStream);
		workbook.close();
		System.out.println("Done");
	}
	
	private static boolean isArrayEquals(JSONArray expected, JSONArray actual) {
		List<Object> expectedList = expected.toList();
		List<Object> actualList = actual.toList();
		if(expectedList.size() != actualList.size()) {
			return false;
		}
		System.out.println(expectedList.size());
		for (Object expectedItem : expectedList) {
			if(!actualList.contains(expectedItem)) {
				return false;
			}
		}
		return true;
	}

	public static Map<String, Object> handleJSONObject(JSONObject jsonObject, String pathName, Map<String, Object>... itemList) {
		Map<String, Object> itemListToUse;
		if (itemList.length == 0) {
			itemListToUse = new HashMap<String, Object>();
		} else {
			itemListToUse = itemList[0];
		}
		Iterator<String> keys = jsonObject.keys();
		while (keys.hasNext()) {
			String key = keys.next();
			String pathNameUse = pathName + "." + key;
			if (jsonObject.get(key) instanceof JSONObject) {
				handleJSONObject((JSONObject) jsonObject.get(key), pathNameUse, itemListToUse);
			} else if (jsonObject.get(key) instanceof JSONArray) {
				//If the JSONArray contains a list of elements (String, numbers...) the add it to map to compare later
				Object result = handleJSONArray((JSONArray) jsonObject.get(key), pathNameUse, itemListToUse);
				if(result != null) {
					pathNameUse = pathNameUse.substring(1);
					itemListToUse.put(pathNameUse, result);
				}
			} else {
				Object nodeValue = jsonObject.get(key);
				pathNameUse = pathNameUse.substring(1);
				if (nodeValue instanceof String) {
					itemListToUse.put(pathNameUse, jsonObject.getString(key));
				} else if (nodeValue instanceof Integer) {
					itemListToUse.put(pathNameUse, jsonObject.getInt(key));
				} else if (nodeValue instanceof Double) {
					itemListToUse.put(pathNameUse, jsonObject.getDouble(key));
				} else if (nodeValue instanceof BigDecimal) {
					itemListToUse.put(pathNameUse, jsonObject.getBigDecimal(key));
				} else if (nodeValue instanceof Long) {
					itemListToUse.put(pathNameUse, jsonObject.getLong(key));
				} else if (nodeValue instanceof Float) {
					itemListToUse.put(pathNameUse, jsonObject.getFloat(key));
				} else if (nodeValue instanceof Boolean) {
					itemListToUse.put(pathNameUse, jsonObject.getBoolean(key));
				} else {
					System.out.println();
					System.out.println("Don't know what data type is: " + key + " - " + nodeValue + " - " + nodeValue.getClass().getName());
				}
			}
		}
		return itemListToUse;
	}

	public static JSONArray handleJSONArray(JSONArray jsonArray, String pathName, Map<String, Object> itemList) {
		//Check if JsonArray contains a list of JSONObject or a list of element (String, number...)
		Object firstItemArray = jsonArray.get(0);
		if (firstItemArray instanceof JSONObject) {
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				handleJSONObject(jsonObject, pathName + i, itemList);
			}
			return null;
		} else {
			return jsonArray;
		}
	}

	public static void writeResult(XSSFSheet sheet, String testID, String testName, boolean isTestCasePassed, String reason, int rowIndex) {
		CellStyle testInfoCellStyle = sheet.getWorkbook().createCellStyle();
		testInfoCellStyle.setBorderBottom(BorderStyle.THIN);
		testInfoCellStyle.setBorderLeft(BorderStyle.THIN);
		testInfoCellStyle.setBorderRight(BorderStyle.THIN);
		testInfoCellStyle.setBorderTop(BorderStyle.THIN);
		testInfoCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		testInfoCellStyle.setWrapText(true);
		Row testCaseResult = sheet.createRow(rowIndex);
		Cell testIDCell = testCaseResult.createCell(0);
		testIDCell.setCellValue(testID);
		testIDCell.setCellStyle(testInfoCellStyle);

		Cell testNameCell = testCaseResult.createCell(1);
		testNameCell.setCellValue(testName);
		testNameCell.setCellStyle(testInfoCellStyle);

		Cell testReasonCell = testCaseResult.createCell(3);
		testReasonCell.setCellValue(reason);
		testReasonCell.setCellStyle(testInfoCellStyle);

		Cell testResult = testCaseResult.createCell(2);
		CellStyle resultCellStyle = sheet.getWorkbook().createCellStyle();
		resultCellStyle.setBorderBottom(BorderStyle.THIN);
		resultCellStyle.setBorderLeft(BorderStyle.THIN);
		resultCellStyle.setBorderRight(BorderStyle.THIN);
		resultCellStyle.setBorderTop(BorderStyle.THIN);
		resultCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		resultCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		if (isTestCasePassed) {
			testResult.setCellValue("Pass");
			resultCellStyle.setFillForegroundColor(IndexedColors.BRIGHT_GREEN.index);
			testResult.setCellStyle(resultCellStyle);
		} else {
			testResult.setCellValue("Fail");
			resultCellStyle.setFillForegroundColor(IndexedColors.RED.index);
			testResult.setCellStyle(resultCellStyle);
		}
	}

	public static void generateHeader(XSSFSheet sheet) {
		CellStyle headerCellStyle = sheet.getWorkbook().createCellStyle();
		headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		headerCellStyle.setFillForegroundColor(IndexedColors.ORANGE.index);
		headerCellStyle.setBorderBottom(BorderStyle.THIN);
		headerCellStyle.setBorderLeft(BorderStyle.THIN);
		headerCellStyle.setBorderRight(BorderStyle.THIN);
		headerCellStyle.setBorderTop(BorderStyle.THIN);
		Row header = sheet.createRow(0);
		Cell testID = header.createCell(0);
		testID.setCellValue("Test case ID");
		testID.setCellStyle(headerCellStyle);

		Cell testName = header.createCell(1);
		testName.setCellValue("Test case Name");
		testName.setCellStyle(headerCellStyle);

		Cell testResult = header.createCell(2);
		testResult.setCellValue("Result");
		testResult.setCellStyle(headerCellStyle);

		Cell testReason = header.createCell(3);
		testReason.setCellValue("Reason");
		testReason.setCellStyle(headerCellStyle);
	}

}
